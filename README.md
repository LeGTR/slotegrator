## Запуск проект

Для запуска проекта необходимо:

+ Cкачать проект
+ Перейти в папку app
+ выполнить команду composer - `composer install` и `composer dump-autoload -o`
+ Вернуться на уровень выше и запустить docker контейнер `docker-compose up --build`

После успешного запуска проект будет открываться по адресу "http://localhost:8081/"

phpMyAdmin будет доступен "http://localhost:8080/"

Вам необходимо будет создать 2 таблицы "slotegrator" и "slotegrator_test"

После создания таблиц открываем консоль php

выполняем следующие команды 
- `php app.php` - эта команда запустит консольное приложение
- `migrate start` - запустит миграцию проекта и создаст необходимые таблицы

Проект готов к использованию. 

### Замечание

Для запуска приложения в качестве агрегатора и перевода данных в JSON достаточно закоментировать 9 строчку в htaccess и расскоментировать 8. 
Либо в файле index.php -> 8 строчка -> $config['api'] - установить значение true
Правила обработки будут такими же как и для обычной работы проекта   

## CMD

немного про консольные команды

в php консоли запускаем `php app.php`

Нам будут доступны несколько команд для взаимодействия 
- list - покажет список доступных команд
- exit - завершает работу приложения
- {команда из спика} - проверяет наличие команды. если команда есть будет написано "Called command", в противном случае "Команда не найдена"
- {команда из спика} help - показывает описание команды (Описание берется из класса - переменная description)
- {команда из спика} start - запускает команду

## Описание команд

- migrate - запускает миграцию и заполняет БД нужными таблицами. Описание таблиц находится в "app\Cli\Commands\Commands\Migrate"
- coupon - Команда создает новый купон для активации. При запуске команды следует заполнить несколько параметров и сохранить изменения.
- prize - создает призы для купонов. можно создать несколько призов под отдельный купон. на выбор будет доступно 3 типа приза (денежный, бонус, предмет) при вводе купона на сайте тип приза будет выбран автоматически.
- prizeitem - создает приз который относится к типу "предмет" и будет выбран автоматически если при вводе купона рандом пал на предмет
- users - создает нового пользователя 
- payment - показывает пользователей которые запросили вывод средств и возможность автоматического вывода для определенного количества пользователей

Для добавления своей команды достаточно создать файл с одноименным классом в папке "app\Cli\Commands\Commands". Новая команда автоматически подтянется в консоли. 



## Тестирование

`ab -k -c 1500 -n 10000 http://localhost:80/`

Document Path:          /

Document Length:        2528 bytes

Concurrency Level:      1500

Time taken for tests:   45.010 seconds

Complete requests:      10000

Failed requests:        743

   (Connect: 0, Receive: 0, Length: 743, Exceptions: 0)

Keep-Alive requests:    9287

Total transferred:      27505592 bytes

HTML transferred:       23523040 bytes

Requests per second:    222.17 [#/sec] (mean)

Time per request:       6751.555 [ms] (mean)

Time per request:       4.501 [ms] (mean, across all concurrent requests)

Transfer rate:          596.77 [Kbytes/sec] received

Connection Times (ms)

              min  mean[+/-sd] median   max

Connect:        0   36 333.8      0    7202

Processing:    18 2767 8139.3    295   44781

Waiting:       18 4086 10363.5    295   44781

Total:         18 2803 8281.2    295   44793


Percentage of the requests served within a certain time (ms)

  50%    295

  66%    300

  75%    303

  80%    316

  90%   5005

  95%  29297

  98%  34764

  99%  39958

 100%  44793 (longest request)

 
 
`ab -k -c 1500 -n 10000 http://localhost:80/prize/`


Document Path:          /prize/

Document Length:        3437 bytes


Concurrency Level:      1500

Time taken for tests:   40.371 seconds

Complete requests:      10000

Failed requests:        547

   (Connect: 0, Receive: 0, Length: 547, Exceptions: 0)

Non-2xx responses:      9474

Keep-Alive requests:    9457

Total transferred:      36465293 bytes

HTML transferred:       32562138 bytes

Requests per second:    247.70 [#/sec] (mean)

Time per request:       6055.629 [ms] (mean)

Time per request:       4.037 [ms] (mean, across all concurrent requests)

Transfer rate:          882.09 [Kbytes/sec] received


Connection Times (ms)

              min  mean[+/-sd] median   max

Connect:        0   21 248.8      0    3120

Processing:    23 1954 6353.5    302   39588

Waiting:       23 2775 8163.3    301   39588

Total:         23 1975 6455.3    302   39606


Percentage of the requests served within a certain time (ms)

  50%    302
  
  66%    306
  
  75%    315
  
  80%    320
  
  90%   5003
  
  95%   7899
  
  98%  30055
  
  99%  35308
  
 100%  39606 (longest request)
 
