<? $this->layout('layout'); ?>

<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
  <div class="row">
    
  <div class="col-md-6">
      <form action="/login" method="POST">
        <div class="mb-3">
          <label for="auth_mail" class="form-label">Почта</label>
          <input type="email" name="mail" class="form-control" id="auth_mail" aria-describedby="auth_mailHelp" value="<?= isset( $auth['mail'] ) ? $auth['mail'] : ''; ?>">
          <div id="auth_mailHelp" class="form-text">Введите почту что указывали при регистрации.</div>
        </div>
        <div class="mb-3">
          <label for="auth_Password1" class="form-label">Пароль</label>
          <input type="password" name="password" class="form-control" id="auth_Password1">
        </div>

        <button type="submit" class="btn btn-primary">Войти</button>
      </form>
    </div>
    
    <div class="col-md-6">
      <form action="/registr" method="POST">
        <div class="mb-3">
          <label for="surname" class="form-label">Фамилия</label>
          <input type="text" name="surname" class="form-control" id="surname"  value="<?= isset( $_SESSION['_POST']['surname'] ) ? $_SESSION['_POST']['surname'] : '';?>">
        </div>
        <div class="mb-3">
          <label for="name" class="form-label">Имя</label>
          <input type="text" name="name" class="form-control" id="name"  value="<?= isset( $_SESSION['_POST']['name'] ) ? $_SESSION['_POST']['name'] : '';?>">
        </div>
        <div class="mb-3">
          <label for="patronymic" class="form-label">Отчество</label>
          <input type="text" name="patronymic" class="form-control" id="patronymic"  value="<?= isset( $_SESSION['_POST']['patronymic'] ) ? $_SESSION['_POST']['patronymic'] : '';?>">
        </div>
        <div class="mb-3">
          <label for="mail" class="form-label">Почта</label>
          <input type="email" name="mail" class="form-control" id="mail" aria-describedby="emailHelp" value="<?= isset( $_SESSION['_POST']['mail'] ) ? $_SESSION['_POST']['mail'] : '';?>">
          <div id="emailHelp" class="form-text">Это будет ваш логин.</div>
        </div>
        <div class="mb-3">
          <label for="password" class="form-label">Пароль</label>
          <input type="password" name="password" class="form-control" id="password">
        </div>
        <div class="mb-3">
          <label for="password_repeat" class="form-label">Пароль (Повторить)</label>
          <input type="password" name="password_repeat" class="form-control" id="password_repeat">
        </div>

        <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
      </form>
    </div>
  </div>
</div>
    
<? if ( isset( $errors ) ) {?>
<?php $this->beginBlock('errors');?>
<div class="container">
  <div class="alert alert-danger">
    <ul>
      <? foreach ($errors  as $key => $value) {?>
        <li><?=$key;?> - <?=$value;?></li>
      <?} ?>
    </ul>
  </div>
</div>
<?php $this->endBlock();?>
<?} ?>
<?php $this->beginBlock('meta');?>
    <meta name="description" content="Регистрация">
<?php $this->endBlock();?>