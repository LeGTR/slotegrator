<? $this->layout('layout'); ?>

<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
  <div class="row">
    <div class="col-md-6">
      <div class="col-md-5 ">
        <h1 class="display-4 fw-normal"><?=$h1;?></h1>
        <p class="lead fw-normal"><?=$description;?></p>
        <? if ( $allCoupons ) {?>
          <ul>
            <? foreach ($allCoupons as $key => $value) {?>
              <li>Название купона:<?=$value['name']?> - <br>введите: <span><?=$value['code']?></span></li>
            <?} ?>
          </ul>
        <?} ?>
      </div>
      <div class="product-device shadow-sm d-none d-md-block"></div>
      <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
    </div>
  <? if ( $auth && $prize ) {?>
    <div class="col-md-6">
      <h3>Ваш подарок</h3>
      <p><b><?=$prize['coupon']?></b></p>
      <p><?=$prize['name']?></p>
      <? if ( !empty( $text ) ) {?>
        <p><?=$text;?></p>
      <?} ?>
      <?=$prize['form_payment'];?>
    </div>
  <?}elseif($auth){?>
    <div class="col-md-6">
      <form action="/prize/select" method="POST">
        <div class="mb-3">
          <label for="code" class="form-label">Купон</label>
          <input type="text" name="code" class="form-control" id="code">
        </div>
        <button type="submit" class="btn btn-primary">Получить подарок</button>
      </form>
    </div>
  <?}else{?>
    <div class="col-md-6">
      <form action="/login" method="POST">
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Почта</label>
          <input type="email" name="mail" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
          <div id="emailHelp" class="form-text">Введите почту что указывали при регистрации.</div>
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">Пароль</label>
          <input type="password" name="password" class="form-control" id="exampleInputPassword1">
        </div>
        <div class="mb-3">
          <a href="/registr">Зарегистрироваться</a>
        </div>
        <button type="submit" class="btn btn-primary">Войти</button>
      </form>
    </div>
  <?} ?>
  </div>
</div>

<? if ( isset( $errors ) ) {?>
  <?php $this->beginBlock('errors');?>
  <div class="container">
    <div class="alert alert-danger">
      <ul>
        <? foreach ($errors  as $key => $value) {?>
          <li><?=$key;?> - <?=$value;?></li>
        <?} ?>
      </ul>
    </div>
  </div>
  <?php $this->endBlock();?>
<?} ?>

<?php $this->beginBlock('meta');?>
    <meta name="description" content="Page Description">
<?php $this->endBlock();?>