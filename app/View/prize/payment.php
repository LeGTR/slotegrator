<div class="row">
    <div class="col-md-6">
        <h2>Вывод средств</h2>
        <form action="/prize/money" method="POST">
            <input type="hidden" name="user_prize_id" value="<?=$user_prize_id;?>">
            <div class="mb-3">
                <label for="bank" class="form-label">Банк</label>
                <input type="text" name="bank" class="form-control" id="bank">
            </div>
            <div class="mb-3">
                <label for="card" class="form-label">Номер карты</label>
                <input type="text" name="card" class="form-control" id="card">
            </div>
            <button type="submit" class="btn btn-primary">Запросить вывод средств</button>
        </form>

    </div>
    <div class="col-md-6">
        <h2>Получить баллы</h2>
        <form action="/prize/bonus" method="POST">
            <button type="submit" class="btn btn-primary">Перевести в баллы</button>
        </form>
    </div>
</div>