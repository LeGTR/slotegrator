<? $this->layout('layout'); ?>

<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
    <div class="col-md-5 p-lg-5 mx-auto my-5">
      <h1 class="display-4 fw-normal"><?=$h1;?></h1>
      <p class="lead fw-normal"><?=$description;?></p>
      <a class="btn btn-outline-secondary" href="/prize"><?=$btn;?></a>
    </div>
    <div class="product-device shadow-sm d-none d-md-block"></div>
    <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
  </div>
    

<?php $this->beginBlock('meta');?>
    <meta name="description" content="Page Description">
<?php $this->endBlock();?>