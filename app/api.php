<?php

require_once __DIR__ . "/vendor/autoload.php";

global $database;
global $config;
$config = require_once __DIR__ . "/config.php";
$config['api'] = true;
$database = $config['database']['default'];
class App 
{

    protected $config;
    protected $db;
    public $router;
    public function __construct(array $config = []) {
        session_start();
        global $database;
        $this->config = $config;
        $this->db = new Core\Db(
            $database['host'],
            $database['user'],
            $database['password'],
            $database['table'],
            $database['port']
        );
        
    }

    public function init()
    {
        if ( !(bool)$this->db->connected() ) {
            die("Ошибка базы данных");
        }

        require_once __DIR__ . "/config/Routes.php";

        $guery = !empty( $_GET['q'] ) ? $_GET['q'] : '/';
        $router->getRoute( $guery );
    }
}

$app = new App($config);
$app->init();