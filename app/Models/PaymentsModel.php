<?php namespace App\Models;

use \Core\Services\Models;

class PaymentsModel extends Models 
{
    protected $table = "payments";
    protected $key = "payment_id";
    protected $allowFields = [
        "user_prize_id",
        "coupon_id",
        "prize_id",
        "user_id",
        "bank",
        "card",
        "result",
        "status",
        "deleted"
    ];
    public function __construct()
    {
        parent::__construct();
        //$this->builder = parent::builder( $this->table, $this->allowFields );
    }

    public function getItems()
    {
        $result = $this->builder->select('*')->get();
        if ( $result->num_rows ) {
            return $result->rows;
        }
        return false;
    }

    public function getItemUser( $user_id = '' )
    {
        $result = $this->select('*')->where("user_id", $user_id)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }

    public function getItemPrize( $prize_id = '' )
    {
        $result = $this->select('*')->where("prize_id", $prize_id)->get();
        if ( $result->num_rows ) {
            return $result->rows;
        }
        return false;
    }

    public function getItemCoupon( $coupon_id = '' )
    {
        $result = $this->select('*')->where("coupon_id", $coupon_id)->get();
        if ( $result->num_rows ) {
            return $result->rows;
        }
        return false;
    }
    
}
