<?php namespace App\Models;

use \Core\Services\Models;

class UsersModel extends Models 
{
    protected $table = "users";
    protected $key = "user_id";
    protected $allowFields = [
        "name",
        "surname",
        "patronymic",
        "mail",
        "password",
        "bonus",
        "status",
        "deleted"
    ];
    public function __construct()
    {
        parent::__construct();
        //$this->builder = parent::builder( $this->table, $this->allowFields );
    }

    public function getItems()
    {
        $result = $this->builder->select('*')->get();
        if ( $result->num_rows ) {
            return $result->rows;
        }
        return false;
    }

    public function getItem( $mail = '' )
    {
        $result = $this->select('*')->where("mail", $mail)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }
    public function getItemId( $user_id = '' )
    {
        $result = $this->select('*')->where("user_id", $user_id)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }
    
}
