<?php namespace App\Models;

use \Core\Services\Models;
class CouponModel extends Models 
{
    protected $table = "coupon";
    protected $key = "coupon_id";
    protected $allowFields = [
        "name",
        "code", // 1 - денежный, 2 - баллы, 3 - подарок
        "count",
        "status",
        "deleted"
    ];
    public function __construct()
    {
        parent::__construct();
    }

    public function getItems()
    {
        $result = $this->select('*')->get();
        if ( $result->num_rows ) {
            return $result->rows;
        }
        return false;
    }
    
    public function getItem( $coupon_id = '' )
    {
        $result = $this->select('*')->where("coupon_id", $coupon_id)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }

    public function findCoupon( $coupon = '' )
    {
        $result = $this->select('*')->where("code", $coupon)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }
    
}
