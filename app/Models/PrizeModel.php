<?php namespace App\Models;

use \Core\Services\Models;
class PrizeModel extends Models 
{
    protected $table = "prize";
    protected $key = "prize_id";
    protected $allowFields = [
        "coupon_id",
        "type", // 1 - денежный, 2 - баллы, 3 - подарок
        "name",
        "status",
        "deleted"
    ];
    public function __construct()
    {
        parent::__construct();
    }

    public function getItems()
    {
        $result = $this->select('*')->get();
        if ( $result->num_rows ) {
            return $result->rows;
        }
        return false;
    }
    
    public function getItem( $prize_id = '' )
    {
        $result = $this->select('*')->where("prize_id", $prize_id)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }

    public function findPrize(int $coupon_id = 0)
    {
        $result = $this->select('*')->where("coupon_id", $coupon_id)->get();
        if ( $result->num_rows ) {
            return $result->rows;
        }
        return false;
    }
}
