<?php namespace App\Models;

use \Core\Services\Models;

class UsersPrizeModel extends Models 
{
    protected $table = "users_prize";
    protected $key = "user_prize_id";
    protected $allowFields = [
        "coupon_id",
        "user_id",
        "prize_id",
        "count",
        "item",
        "status", // 1 получен, 2 запрошен вывод средств, 3 переведено из рублей в баллы, 4 отправлен почтой
        "deleted"
    ];
    public function __construct()
    {
        parent::__construct();
        //$this->builder = parent::builder( $this->table, $this->allowFields );
    }

    public function getItems()
    {
        $result = $this->builder->select('*')->get();
        if ( $result->num_rows ) {
            return $result->rows;
        }
        return false;
    }

    public function getItemPosition( $user_prize_id = '' )
    {
        $result = $this->select('*')->where("user_prize_id", $user_prize_id)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }
    
    public function getItem( $user_id = '' )
    {
        $result = $this->select('*')->where("user_id", $user_id)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }

    public function getItemPrize( $prize_id = '' )
    {
        $result = $this->select('*')->where("prize_id", $prize_id)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }

    public function find( $user_id,  $prize_id)
    {
        $result = $this->select('*')->where("user_id", $user_id)->where("prize_id", $prize_id)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }
    
}
