<?php namespace App\Models;

use \Core\Services\Models;
class PrizeItemsModel extends Models 
{
    protected $table = "prize_items";
    protected $key = "prize_item_id";
    protected $allowFields = [
        "prize_id",
        "name",
        "count",
        "status",
        "deleted"
    ];
    public function __construct()
    {
        parent::__construct();
    }

    public function getItems()
    {
        $result = $this->select('*')->get();
        if ( $result->num_rows ) {
            return $result->rows;
        }
        return false;
    }
    
    public function getItem( $prize_id = '' )
    {
        $result = $this->select('*')->where("prize_id", $prize_id)->get();
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }

    public function findPrize(int $coupon_id = 0)
    {
        $result = $this->select('*')->where("coupon_id", $coupon_id)->get();
        if ( $result->num_rows ) {
            return $result->rows;
        }
        return false;
    }

    public function randomItem()
    {
        $result = $this->select('*')->where("count >", 0)->rand(1);
        if ( $result->num_rows ) {
            return $result->row;
        }
        return false;
    }
}
