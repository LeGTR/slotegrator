<?php

require_once __DIR__ . "/../vendor/autoload.php";
use \PHPunit\Framework\TestCase;
use Core\Db;

var_dump( class_exists( "TestCase" ) );
class DbTest extends TestCase
{
    protected $db;
    public function setUp(): void
    {
        $this->db = new Db();
    }

    public function testConnected()
    {
        $result = $this->db->connected();
        $this->assertEquals(1, (int)$result);
    }
}
