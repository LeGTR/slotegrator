<?php
namespace Core\Services;

use Core\Db;

class Build
{

    protected $conn; 
    private $select = [];
    private $where = [];
    private $orwhere = [];
    private $order = [];
    private $join = [];

    protected $table = '';
    protected $limit = null;
    protected $start = null;
    protected $allowFields = [];
    protected $builder;

    private $sql = '';

    public function __construct() {
        $this->conn = new \Core\Db();
    }
    

    public function select( $data )
    {
        if ( is_array( $data ) ) {
            foreach ($data as $key => $value) {
                if ( !empty($this->allowFields) && !in_array($this->conn->escape($value), $this->allowFields) ) {
                    continue;
                }
                $this->select[] = $value;
            }
        }elseif( is_string($data) ){
            if ( !empty($this->allowFields) && !in_array($data, $this->allowFields) && $data != "*") {
                return $this;
            }
            $this->select[] = $data;
        }
        return $this;
    }

    public function where($field = '', $value = '')
    {
        if ( !empty( $field ) && $value !== '' ) {
            $operator = $this->getOperator($field);
            $this->where[] = $this->conn->escape( str_replace( (string)$operator, '', $field) ) . (string)$operator .  '"' . $this->conn->escape($value) . '"';
        }
        return $this;
    }
    
    public function orwhere($field = '', $value = '')
    {
        if ( !empty( $field ) && !empty($value) ) {
            $operator = $this->getOperator($field);
            $this->orwhere[] = $this->conn->escape( str_replace( (string)$operator, '', $field) ) . (string)$operator .  '"' . $this->conn->escape($value) . '"';
        }
        return $this;
    }

    public function order($field = '', $sort = 'DESC')
    {
        if ( !empty( $field ) ) {
            $this->order[] = $this->conn->escape($field) . ' ' . $this->conn->escape($sort);
        }
        return $this;
    }

    public function join($table = '', $condition = '', $type = 'LEFT')
    {
        if ( !empty( $table ) && !empty( $keys ) ) {
            $this->join[] = $this->conn->escape($type) . ' JOIN ' . $this->conn->escape($table) . ' ON ' . $this->conn->escape($condition); 
        }
        return $this;
    }

    public function limit($limit)
    {
        $this->limit = (int)$limit;
    }

    public function start($start)
    {
        $this->start = (int)$start;
    }

    public function get($start = 0, $limit = 20, $rand = false)
    {
        $this->sql = "SELECT ";
        $this->sql .= implode(',', $this->select);
        $this->sql .= " FROM " . $this->table;
        if ( !empty( $this->join ) ) {
            $this->sql .= implode(' ', $this->join);
        }
        if ( !empty( $this->where ) ) {
            $this->sql .= " WHERE " . implode(' AND ', $this->where);
        }
        if ( !empty( $this->orwhere ) ) {
            if ( empty( $this->where ) ) {
                $this->sql .= " WHERE ";
            }
            $this->sql .=  ' ('. implode(' OR ', $this->orwhere) .') ';
        }
        if ( $rand ) {
            $this->sql .= " ORDER BY RAND() ";
        }elseif ( !empty( $this->order ) ) {
            $this->sql .= " ORDER BY " . implode(',', $this->order);
        }

        if ( !is_null( $this->start ) ) {
            $this->sql .= " LIMIT " . $this->start;
        }else{
            $this->sql .= " LIMIT " . $start;
        }
        
        if ( !is_null( $this->limit ) ) {
            $this->sql .= "," . $this->limit;
        }elseif( !empty( $limit ) ){
            $this->sql .= "," . $limit;
        }

        return $this->getResult();
    }

    public function rand($limit = 1)
    {
        return $this->get($limit, null, true);
    }

    public function getResult()
    {
        $result = $this->conn->query( $this->sql );
        if ( isset( $result->num_rows ) ) {
            return $result;
        }
        return false;
    }

    public function save( $data = [] )
    {
        $insert = [];
        if ( !empty( $data ) ) {
            foreach ($data  as $key => $value) {
                if ( in_array( $key, $this->allowFields ) ) {
                    $insert[] = $this->conn->escape($key) . " = '". $this->conn->escape($value) ."'";
                }
            }
        }

        $sql = " INSERT INTO `". $this->table ."` SET ". implode(', ', $insert) ;
        $this->conn->query($sql);
    }

    public function update( $data = [] )
    {
        if ( isset( $data[$this->key] ) ) {
            $_key = [
                "key" => $this->key,
                "value" => $data[ $this->key ]
            ];
        }else{
            return false;
        }
        $insert = [];
        if ( !empty( $data ) ) {
            foreach ($data  as $key => $value) {
                if ( in_array( $key, $this->allowFields ) ) {
                    $insert[] = $this->conn->escape($key) . " = '". $this->conn->escape($value) ."'";
                }
            }
        }

        $sql = " UPDATE `". $this->table ."` SET ". implode(', ', $insert) . " WHERE " . $_key['key'] . " = " . $_key['value'];
        $this->conn->query($sql);
    }

    protected function getOperator(string $str, bool $list = false)
    {
        $pregOperators = [
            '\s*(?:<|>|!)?=\s*', // =, <=, >=, !=
            '\s*<>?\s*', // <, <>
            '\s*>\s*', // >
            '\s+IS NULL', // IS NULL
            '\s+IS NOT NULL', // IS NOT NULL
            '\s+EXISTS\s*\(.*\)', // EXISTS(sql)
            '\s+NOT EXISTS\s*\(.*\)', // NOT EXISTS(sql)
            '\s+BETWEEN\s+', // BETWEEN value AND value
            '\s+IN\s*\(.*\)', // IN(list)
            '\s+NOT IN\s*\(.*\)', // NOT IN (list)
        ];

        return preg_match_all(
            '/' . implode('|', $pregOperators) . '/i',
            $str,
            $match
        ) ? ($list ? $match[0] : $match[0][0]) : " = ";
    }
    
}
