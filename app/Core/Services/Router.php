<?php
namespace Core\Services;

class Router 
{

    private $routes = [
        "get" => [],
        "post" => [],
        "put" => [],
        "delete" => [],
    ];
    private $type = [ 
        "get",
        "post",
        "put",
        "delete"
    ];
    private $links = [
        "get" => [],
        "post" => [],
        "put" => [],
        "delete" => []
    ];
    private $name = '';

    public function __construct() {
        $this->routes['get']['default'] = [
            "name" => "",
            "controller" => "Home",
            "method" => "index",
            "filter" => false
        ];
    }

    public function get(string $name = "", string $controller = "", $filter = false)
    {
        return $this->uri("get", $name, $controller, $filter);
    }

    public function post(string $name = "", string $controller = "", $filter = false)
    {
        return $this->uri("post", $name, $controller, $filter);
    }
    public function put(string $name = "", string $controller = "", $filter = false)
    {
        return $this->uri("put", $name, $controller, $filter);
    }
    public function delete(string $name = "", string $controller = "", $filter = false)
    {
        return $this->uri("delete", $name, $controller, $filter);
    }

    public function uri(string $type = 'get', string $name = "", string $controller = "", $filter = false)
    {
        if ( !in_array( $type, $this->type ) ) {
            return false;
        }
        if ( !isset( $this->routes[$type][$name] ) ) {
            $_controller = explode("::", $controller);
            $this->routes[$type][$name] = [
                "name" => $name,
                "controller" => $_controller[0],
                "method" => isset( $_controller[1] ) ? $_controller[1] : "index",
                "filter" => $filter
            ];
            $this->links[$type][$name] = $name;
            $this->name = $name;
            $this->type_link = $type;
        }

        return $this;
    }

    public function link( string $link = '' )
    {
        if ( !empty( $link ) && !empty( $this->name ) && isset( $this->links[$this->type_link][$this->name] ) ) {
            $type = $this->links[$this->name]['type'];
            unset($this->links[$this->type_link][$this->name]);
            $this->links[$this->type_link][$link] = $this->name;
        }
        $this->name = '';
        $this->type_link = '';
    }

    public function getRoute(string $uri = '/')
    {
        $_get = [];
        $_links = [];
        switch ( $_SERVER['REQUEST_METHOD'] ) {
            case 'GET':
                $_get = $this->routes['get'];
                $_links = $this->links['get'];
                break;
            case 'POST':
                $_get = $this->routes['post'];
                $_links = $this->links['post'];
                break;
            case 'PUT':
                $_get = $this->routes['put'];
                $_links = $this->links['put'];
                break;
            case 'DELETE':
                $_get = $this->routes['delete'];
                $_links = $this->links['delete'];
                break;
            
            default:
                # code...
                break;
        }

        
        require_once __DIR__."/../../Controllers/BaseController.php";
        if ( $uri == '/' || empty( $uri ) ) {
            $controller = "default";
            if ( isset( $_get[$controller] ) ) {
                $class = $_get[$controller];
                if ( $_get[$controller]['filter'] ) {
                    $filter = $this->filter( $_get[$controller]['filter'] );
                }
                $this->findController($class);
                if ( $_get[$controller]['filter']['end'] ) {
                    $filter = $this->filter_end( $filter );
                }
            }
        }else{
            //$url = explode('/', $uri);
            $url[0] = $uri;
            if ( isset( $_get[$url[0]] )) {
                $class = $_get[$url[0]];
                if ( $_get[$url[0]]['filter'] ) {
                    $filter = $this->filter( $_get[$url[0]]['filter'] );
                }
                $this->findController($class);
                if ( $_get[$url[0]]['filter']['end'] ) {
                    $filter = $this->filter_end( $filter );
                }
            }/*elseif( isset( $_links[$url[0]] ) && isset($_get[ $_links[$url[0]] ]) ){
                $class = $_get[ $_links[$url[0]]];
                $this->findController($class);
            }*/else{
                $class = $url[0];
                $method = isset( $url[1] ) ? $url[1] : 'index';
                if ( count( $url ) > 2 ) {
                    unset( $url[0], $url[1] );
                    $attr = $url;
                }
                if ( $_get[$class]['filter'] ) {
                    $filter = $this->filter( $_get[$class]['filter'] );
                }
                $this->scanController($class, $method);
                if ( $_get[$class]['filter']['end'] ) {
                    $filter = $this->filter_end( $filter );
                }
            }
        }
    }

    private function findController(array $class = [])
    {
        if ( file_exists( "Controllers/" . Ucfirst($class['controller']) . ".php" ) ) {
            return $this->load($class['controller'], $class['method']);
        }
    }

    private function scanController(string $class = '', string $method = 'index')
    {
        $controllers = scandir("Controllers/");
        foreach ($controllers as $key => $value) {
            if ( Ucfirst($class) . '.php' == $value ) {
                return $this->load($class, $method);
            }
        }
        return $this->load404();
    }

    private function load(string $class = '', string $method = 'index')
    {
        require_once "Controllers/" . Ucfirst($class) . ".php";
        $_class = 'App\\Controllers\\'. Ucfirst($class);
        $action = new $_class();
        $method = $method;
        if ( method_exists( $action, $method ) ) {
            return $action->$method();
        }else{
            return $this->load404();
        }
    }

    public function filter( $name )
    {
        require_once "Filter/" . Ucfirst($name['start']) . ".php";
        $_class = 'App\\Filter\\'. Ucfirst($name['start']);
        $filter = new $_class;
        $filter->start();
        return $filter;
    }

    public function filter_end($filter)
    {
        if( is_object( $filter ) ){
            $filter->after();
        }
    }

    public function load404()
    {
        require_once "Controllers/Errors.php";
        $action = new \App\Controllers\Errors();
        return $action->error404();

    }

    public function sendLink()
    {
        var_dump( $$links );
    }

    public function getAllGet()
    {
        return $this->routes['get'];
    }

    public function getAllPost()
    {
        return $this->routes['post'];
    }

    public function getAllPut()
    {
        return $this->routes['put'];
    }

    public function getAllDelete()
    {
        return $this->routes['delete'];
    }
    
}
