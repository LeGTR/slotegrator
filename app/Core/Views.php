<?php
namespace Core;
use Devanych\View\Renderer;

class Views 
{
    private $type_file = ".php";
    private $json = false;

    public function __construct() {
    }

    public function view(string $view = '', array $data = [], bool $return = false, bool $json = false)
    {
        if ( $json || $this->$json ) {
            return json_encode($data);
        }

        $phpView = new Renderer("View/");
        return $phpView->render($view.$this->type_file, $data);
    }

}