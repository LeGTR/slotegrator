<?php namespace Core\Filters;

interface FilterInterface
{
    /**
     * Обработка перед вызовом контроллера
     */
    public function start();
    
    /**
     * Сбор данных в контроллер (ограничения и прочее)
     */
    public function init();

    
    /**
     * Обработка после контроллера
     */
    public function after();
}
