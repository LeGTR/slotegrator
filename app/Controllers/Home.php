<?php
namespace App\Controllers;
use App\Controllers\BaseController;

class Home extends BaseController
{
    public function index()
    {
        return $this->view("home/index", [
            "h1" => "Розыгрыш призов",
            "description" => "Вы можете получить деньги, баллы или подарок",
            "btn" => "Получить подарок",
        ]);
    }

    public function post()
    {
        return $this->view("home/index", ["test" => "post"]);
    }

    public function put()
    {
        return $this->view("home/index", ["test" => "put"]);
    }

    public function delete()
    {
        return $this->view("home/index", ["test" => "delete"]);
    }

    public function test() 
    {
        echo "test";
    }

}
