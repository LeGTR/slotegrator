<?php
namespace App\Controllers;
use App\Controllers\BaseController;

use App\Models\CouponModel;
use App\Models\PrizeModel;
use App\Models\PrizeItemsModel;
use App\Models\UsersModel;
use App\Models\UsersPrizeModel;

class Prize extends BaseController
{
    public function index()
    {
        
        $text = '';
        $user = [];
        $auth = $_SESSION['auth'];
        if ( isset( $_SESSION['user'] ) ) {
            $user = $_SESSION['user'];
        }
        $_prize = false;
        if ( isset( $_SESSION['prize'] ) && $_SESSION['prize'] ) {
            $_prize['form_payment'] = '';
            $prize = new \App\Models\PrizeModel();
            $_prize = $prize->getItem( $_SESSION['prize'] );
            $userprize = new UsersPrizeModel();
            $user_prize_id  = $userprize->find($_SESSION['user']['user_id'], $_SESSION['prize'] );
            switch ($_prize['type']) {
                case 1:
                    $_prize['coupon'] = "денежный";
                    switch ( $user_prize_id['status'] ) {
                        case 1:
                            $_prize['form_payment'] = $this->view("prize/payment",["user_prize_id" => $user_prize_id['user_prize_id']], true);
                            break;
                        case 2:

                            $payments = new \App\Models\PaymentsModel();
                            $_payments = $payments->getItemUser($_SESSION['user']['user_id']);
                            if ( $_payments ) {
                                if ( $_payments['result'] == "wait" ) {
                                    $text = "Ожидается вывод средств";
                                }
                                if ( $_payments['result'] == "success" ) {
                                    $text = "Деньги выведены";
                                }
                                if ( $_payments['result'] == "error" ) {
                                    $text = "Во время вывода произошла ошибка, свяжитесь с оператором";
                                }
                            }
                            $_prize['form_payment'] = $this->view("prize/output_money",["user_prize_id" => $user_prize_id['user_prize_id'], "text" => $text], true);
                            break;
                        case 3:
                            $_prize['form_payment'] = $this->view("prize/reload_bonus",["user_prize_id" => $user_prize_id['user_prize_id']], true);
                            break;
                        case 4:
                            $_prize['form_payment'] = $this->view("prize/shipping",["user_prize_id" => $user_prize_id['user_prize_id']], true);
                            break;
                        
                        default:
                            # code...
                            break;
                    }
                    break;
                case 2:
                    $_prize['coupon'] = "баллы";
                    $text = "Вы получили <b>". $user_prize_id['count'] ."</b> баллов. они были зачислены на ваш Акк";
                    break;
                case 3:
                    $_prize['coupon'] = "подарок";
                    $text = "Вы выиграли <b>". $user_prize_id['item'] ."!</b> Менеджер свяжется с вами для уточнения деталей";
                    break;
                
                default:
                    $_prize['coupon'] = "";
                    break;
            }

        }
        $coupon = new CouponModel();
        $allCoupons = $coupon->getItems();

        return $this->view("prize/index", [
            "h1" => "Розыгрыш призов",
            "user" => $user,
            "allCoupons"  => $allCoupons,
            "auth"  => $auth,
            "prize"  => $_prize,
            "text"  => $text,
            "description" => "Вы можете получить деньги, баллы или подарок",
            "btn" => "Получить подарок",
        ]);
    }

    public function post()
    {
        return $this->view("prize/index", ["test" => "post"]);
    }

    public function put()
    {
        return $this->view("prize/index", ["test" => "put"]);
    }

    public function delete()
    {
        return $this->view("prize/index", ["test" => "delete"]);
    }
    
    public function select()
    {
        global $config;
        if ( !$_SESSION['prize'] ) {
            $coupon = new CouponModel();
            $_coupon = $coupon->findCoupon( $_POST['code'] );
            if ( !$_coupon ) {
                $_SESSION['errors'] = [
                    "code" => "Купон не найден"
                ];
                header("Location:/prize");
            }


            $prize = new PrizeModel();
            $_prize = $prize->findPrize( $_coupon['coupon_id'] );
            if ( !$_prize ) {
                $_SESSION['errors'] = [
                    "code" => "Подарок не найден(("
                ];
                header("Location:/prize");
            }
            
            $rand_keys = array_rand($_prize, 1);
            $select_prize = $_prize[$rand_keys];

            $data = [
                "user_id" => $_SESSION['user']['user_id'],
                "coupon_id" => $_coupon['coupon_id'],
                "prize_id" => $select_prize['prize_id'],
                "status" => 1,
                "deleted" => 0,
            ];

            
            switch ($select_prize['type']) {
                case 1:
                    $money_random = rand((int) $config["min_money"], (int) $config["max_money"]);
                    $data['count'] = $money_random;
                    break;
                case 2:
                    $bonus_random = rand((int) $config["min_bonus"], (int) $config["max_bonus"]);
                    $data['count'] = $bonus_random;

                    $_user_model = new UsersModel();
                    $_user_bonus = $_user_model->getItemId( $_SESSION['user']['user_id'] );

                    $user_data = [
                        "user_id" => $_SESSION['user']['user_id'],
                        "bonus" => $_user_bonus["bonus"] + $bonus_random
                    ];
                    $_user_model->update( $user_data );
                    break;
                case 3:
                    $data['count'] = 1;

                    $prize_item = new PrizeItemsModel();
                    $_prize = $prize_item->randomItem();
                    $data['item'] = $_prize['name'];
                    $_prize['count'] -=1;
                    $prize_item->update( $_prize );
                    break;
                
                default:
                    # code...
                    break;
            }

            
            $userprize = new UsersPrizeModel();
            $userprize->save( $data );
            header("Location:/prize");
        }
    }

    public function money_out()
    {

        $userprize = new UsersPrizeModel();
        $user_prize = $userprize->getItemPosition( $_POST['user_prize_id'] );
        if ( $user_prize['status'] == 1 ) {
            $user_prize['status'] = 2;

            $payment = new \App\Models\PaymentsModel();
            $data = [
                "user_prize_id"     => $user_prize['user_prize_id'],
                "coupon_id"         => $user_prize['coupon_id'],
                "prize_id"          => $user_prize['prize_id'],
                "user_id"           => $user_prize['user_id'],
                "bank"              => $_POST['bank'],
                "card"              => $_POST['card'],
                "result"            => 'wait',
                "status"            => '1',
                "deleted"           => '0'
            ];
            $payment->save( $data );

            $userprize->update($user_prize);
            
        }
        header("Location:/prize");

    }

    public function bonus_in()
    {
        $userprize = new UsersPrizeModel();
        $user_prize = $userprize->getItemPosition( $_POST['user_prize_id'] );
        if ( $user_prize['status'] == 1 ) {
            $user_prize['status'] = 3;

            $users = new \App\Models\UsersModel();
            $_users = $users->getItem( $user_prize['user_id'] );
            $data = [
                "user_id"           => $user_prize['user_id'],
                "bonus"             => $_users['bonus'] + $user_prize['count']
            ];
            $users->update( $data );

            $userprize->update($user_prize);
            
        }
        header("Location:/prize");
    }

}
