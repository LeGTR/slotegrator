<?php
namespace App\Controllers;

use Devanych\View\Renderer;


class BaseController
{
    private $type_file = ".php";
    private $json = false;

    public function view(string $view = '', array $data = [], bool $return = false, bool $json = false)
    {
        global $config;

        if ( isset( $_SESSION['errors'] ) ) {
            $data['errors'] = $_SESSION['errors'];
            unset( $_SESSION['errors'] );
        }

        if ( isset( $_SESSION['auth'] ) ) {
            $data['auth'] = $_SESSION['auth'];
        }

        if ( $json || $this->json || $config['api'] ) {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($data);
            exit();
        }

        $phpView = new Renderer("View/");
        if ( $return ) {
            return $phpView->render($view.$this->type_file, $data);
        }
        echo $phpView->render($view.$this->type_file, $data);
    }
}
