<?php
namespace App\Controllers;

class Errors extends BaseController
{
    public function error404()
    {
        http_response_code(404);
        return $this->view("404", [
            "h1" => "Страница не найдена"
        ]);
    }
}
