<?php
namespace App\Controllers;
use App\Controllers\BaseController;

class Auth extends BaseController
{
    public function index()
    {
        return $this->view("auth/index", [
            "h1" => "Розыгрыш призов",
            "description" => "Вы можете получить деньги, баллы или подарок",
            "btn" => "Получить подарок",
        ]);
    }

    public function post()
    {
        return $this->view("auth/index", ["test" => "post"]);
    }

    public function put()
    {
        return $this->view("auth/index", ["test" => "put"]);
    }

    public function delete()
    {
        return $this->view("auth/index", ["test" => "delete"]);
    }

    public function login_post()
    {
        $post = $_POST;
        $user = new \App\Models\UsersModel();
        $_user = $user->getItem( $post['mail'] );

        $_SESSION['auth']['mail'] = $post['mail'];

        if ( !password_verify($post['password'], $_user['password']) ) {
            $_SESSION['errors'] = [
                "password" => "пароли не совпадают"
            ];
            header("Location:/registr");
        }

        $_SESSION['user'] = $_user;

        
        header("Location:/prize");
    }

    public function registr()
    {
        if ( isset( $_SESSION['user'] ) ) {
            header("Location:/prize");
        }
        return $this->view("auth/registr", ["test" => "delete"]);
    }

    public function registr_post()
    {
        
        if ( isset( $_SESSION['errors'] ) ) {
            header("Location:/registr");
        }

        
        $user = new \App\Models\UsersModel();
        $user->save( $_POST );
        unset( $_SESSION['_POST'] );
        header("Location:/prize");
    }

}
