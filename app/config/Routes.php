<?php
namespace App\Config\Routes;
use Core\Services\Router;

$router = new Router();

$router->get("home", "Home::index")->link("home");
$router->post("home", "Home::post")->link("home");
$router->put("home", 'Home::put')->link("home");
$router->delete("home", 'Home::delete')->link("home");

$router->get("registr", 'Auth::registr')->link("registr");
$router->post("registr", 'Auth::registr_post', ["start" => "Registr"])->link("registr");
$router->post("login", 'Auth::login_post')->link("registr");


$router->get("prize", 'Prize::index')->link("prize");

$router->post("prize/select", 'Prize::select', ["start" => "Prize"])->link("prize");

$router->post("prize/money", 'Prize::money_out', ["start" => "Prize"])->link("prize");
$router->post("prize/bonus", 'Prize::bonus_in', ["start" => "Prize"])->link("prize");