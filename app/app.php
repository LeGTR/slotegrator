<?php
require_once "Cli/Cli.php";


$config = require_once __DIR__ . "/config.php";
global $db;
global $db_test;
global $testing;
$testing = false;
$db = new Core\Db(
    $config['database']['default']['host'],
    $config['database']['default']['user'],
    $config['database']['default']['password'],
    $config['database']['default']['table'],
    $config['database']['default']['port']
);
if ( isset( $config['database']['tests']['host'] ) ) {
    $testing = true;
    $db_test = new Core\Db(
        $config['database']['tests']['host'],
        $config['database']['tests']['user'],
        $config['database']['tests']['password'],
        $config['database']['tests']['table'],
        $config['database']['tests']['port']
    );
}

$cli = new CLI;
$cli->init($argc, $argv);