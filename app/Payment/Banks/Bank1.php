<?php namespace App\Payment\Banks;

use App\Payment\Payment;
use App\Payment\PaymentInterface;

class Bank1 extends Payment implements PaymentInterface
{
    
    public function connected(  ){
        /** Здесь должны быть функция соединения с банком */
        return true;
    }

    public function payment($card, $amount){
        /** Здесь должны быть функция отправки запроса на вывод средства и возврат результата */

        return true;
    }
}