<?php namespace App\Payment;

interface PaymentInterface
{

    public function connected();

    public function payment($card, $amount);
    
}
