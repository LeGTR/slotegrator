<?php namespace App\Payment;

use App\Payment\PaymentInterface;

class Payment implements PaymentInterface
{

    protected $class = [];

    protected $stop = [
        ".", 
        '..'
    ];

    public function __construct() {
        
        $controllers = scandir(__DIR__ . "/Banks/");
        foreach ($controllers as $key => $value) {
            if ( in_array( $value, $this->stop ) ) {
                continue;
            }
            $this->load($value);
        }

    }
    
    private function load(string $class = '')
    {
        $__class = explode('.', $class)[0];
        require_once __DIR__ . "/Banks/" . Ucfirst($__class) . ".php";
        $_class = 'App\\Payment\\Banks\\'. Ucfirst($__class);
        $this->class[Ucfirst($__class)] = $_class;
    }

    public function connected()
    {
    }

    public function findClass( $class = '' )
    {
        if ( isset( $this->class[Ucfirst($class)] ) && class_exists( $this->class[Ucfirst($class)] ) ) {
            return $this->class[Ucfirst($class)];
        }

        return false;
    }

    public function payment($card, $amount)
    {

    }
}
