<?php 
namespace Cli\Commands;

interface CommandInterface
{
    public function help();
}