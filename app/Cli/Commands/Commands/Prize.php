<?php


class Prize 
{
    public $name = 'Prize';
    public $arguments = [];
    public $options = [];
    public $description = 'Создает призы для купонов';
    
    private $_coupon_id;
    private $_type;
    private $_name;

    private $db;
    private $db_test;
    private $testing;

    private $type_prize = [
        "1" => "денежный приз",
        "2" => "Баллы",
        "3" => "Подарок"
    ];

    private $init;

    public function __construct() {
        global $db;
        global $db_test;
        global $testing;
        $this->db = $db;
        $this->db_test = $db_test;
        $this->testing = $testing;
        $this->init = TRUE;
    }

    public function help()
    {
        \Cli::prompt( $this->description );
    }
    
    public function run()
    {

        $_coupon = $this->select_coupon();
        if ( !$_coupon ) {
            return false;
        }
        while ($this->init) {
            \Cli::prompt( "Введите номер купона (необходимо ввести номер): " );
            $this->viewList($_coupon);
            $this->_coupon_id = \Cli::input();
            
            \Cli::prompt( "Введите тип приза (необходимо ввести номер): " );
            $this->viewList($this->type_prize);
            $this->_type = \Cli::input();

            \Cli::prompt( "Введите название приза: " );
            \Cli::newLine();
            $this->_name = \Cli::input();

            \Cli::newLine();
            \Cli::prompt( "Сохранить (y/n)?" );

            if ( \Cli::input() == "y" ) {
                $sql = "INSERT INTO `prize`(`coupon_id`, `type`, `name`, `status`, `deleted`) VALUES ('". $this->_coupon_id ."','". $this->_type ."','". $this->_name ."','1','0')";
                if ( $this->testing ) {
                    $this->db_test->query( $sql );
                    \Cli::newLine();
                    \Cli::prompt( "Создание записи в тестовой БД" );
                }
        
                \Cli::newLine();
                \Cli::prompt( "Создание записи в БД" );
                $this->db->query( $sql );
            }
            $this->_coupon_id = "";
            $this->_type = "";
            $this->_name = "";

            \Cli::clear();

            \Cli::prompt( "Добавить еще один приз (y/n)?" );
            \Cli::newLine();
            if ( \Cli::input() != "y" ) {
                $this->init = false;
                \Cli::prompt( "Добавление призов завершено" );
                \Cli::newLine();
            }

        }

    }

    public function select_coupon()
    {
        $sql = "SELECT * FROM `coupon`";
        $coupon = $this->db->query( $sql );
        $_coupon = [];
        if ( !$coupon->num_rows ) {
            \Cli::prompt( "Сперва необходимо добавить купон: " );
            return false;
        }
        foreach ($coupon->rows as $key => $value) {
            $_coupon[$value['coupon_id']] = $value['name'];
        }

        return $_coupon;
    }

    public function viewList( array $_coupon = [])
    {
        foreach ($_coupon as $key => $value) {
            \Cli::newLine();
            \Cli::prompt( "$key - $value" );
        }
        \Cli::newLine(2);
    }

    

}
