<?php
class MigrateTableUsers extends Migrate
{

    public function up()
    {
        $this->addField([
                'user_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                        'auto_increment' => true,
                ]
        ]);
        $this->addField([
                'name'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        $this->addField([
                'surname'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        $this->addField([
                'patronymic'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        $this->addField([
                'mail'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        $this->addField([
                'password'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        
        $this->addField([
                'bonus'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                ]
        ]);
        
        $this->addField([
                'status'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        $this->addField([
                'deleted'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);

        $this->addKey('user_id', true);
        $this->createTable('users');
    }

    public function down()
    {
        # code...
    }
    
}
