<?php
class MigratePrizeItems extends Migrate
{

    public function up()
    {
        $this->addField([
                'prize_item_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                        'auto_increment' => true,
                ]
        ]);
        $this->addField([
                'prize_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);

        $this->addField([
                'name'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        $this->addField([
                'count'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        
        $this->addField([
                'status'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        
        $this->addField([
                'deleted'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);

        $this->addKey('prize_item_id', true);
        $this->createTable('prize_items');
    }

    public function down()
    {
        # code...
    }
    
}
