<?php
class MigratePrize extends Migrate
{

    public function up()
    {
        $this->addField([
                'prize_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                        'auto_increment' => true,
                ]
        ]);
        $this->addField([
                'coupon_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);

        $this->addField([
                'type'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);

        $this->addField([
                'name'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        
        $this->addField([
                'status'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        
        $this->addField([
                'deleted'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);

        $this->addKey('prize_id', true);
        $this->createTable('prize');
    }

    public function down()
    {
        # code...
    }
    
}
