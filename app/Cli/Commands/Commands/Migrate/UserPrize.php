<?php
class MigrateUserPrize extends Migrate
{

    public function up()
    {
        $this->addField([
                'user_prize_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                        'auto_increment' => true,
                ]
        ]);
        $this->addField([
                'user_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                ]
        ]);
        $this->addField([
                'coupon_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                ]
        ]);
        $this->addField([
                'prize_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                ]
        ]);
        $this->addField([
                'count'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                ]
        ]);
        
        $this->addField([
                'item'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        
        $this->addField([
                'status'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        $this->addField([
                'deleted'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);

        $this->addKey('user_prize_id', true);
        $this->createTable('users_prize');
    }

    public function down()
    {
        # code...
    }
    
}
