<?php
class MigrateCoupon extends Migrate
{

    public function up()
    {
        $this->addField([
                'coupon_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                        'auto_increment' => true,
                ]
        ]);
        $this->addField([
                'name'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        $this->addField([
                'code'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        
        $this->addField([
                'count'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        
        $this->addField([
                'status'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        $this->addField([
                'deleted'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);

        $this->addKey('coupon_id', true);
        $this->createTable('coupon');
    }

    public function down()
    {
        # code...
    }
    
}
