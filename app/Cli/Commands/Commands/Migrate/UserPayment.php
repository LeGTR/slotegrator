<?php
class MigrateUserPayment extends Migrate
{

    public function up()
    {
        $this->addField([
                'payment_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 8,
                        'auto_increment' => true,
                ]
        ]);
        $this->addField([
                'user_prize_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        $this->addField([
                'coupon_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        $this->addField([
                'prize_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);

        $this->addField([
                'user_id'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        
        $this->addField([
                'bank'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);

        $this->addField([
                'card'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);

        $this->addField([
                'result'          => [
                        'type'           => 'VARCHAR',
                        'constraint'     => 255,
                ]
        ]);
        
        $this->addField([
                'status'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);
        $this->addField([
                'deleted'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                ]
        ]);

        $this->addKey('payment_id', true);
        $this->createTable('payments');
    }

    public function down()
    {
        # code...
    }
    
}
