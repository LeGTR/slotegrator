<?php


class Coupon 
{
    public $name = 'Coupon';
    public $arguments = [];
    public $options = [];
    public $description = 'Создает купон';
    
    private $_name;
    private $_code;
    private $_count;

    private $db;
    private $db_test;
    private $testing;

    private $init;

    public function __construct() {
        global $db;
        global $db_test;
        global $testing;
        $this->db = $db;
        $this->db_test = $db_test;
        $this->testing = $testing;
        $this->init = TRUE;
    }

    public function help()
    {
        \Cli::prompt( $this->description );
    }
    
    public function run()
    {
        while ($this->init) {
            \Cli::prompt( "Введите название купона: " );
            \Cli::newLine();
            $this->_name = \Cli::input();
            
            \Cli::prompt( "Введите код купона: " );
            \Cli::newLine();
            $this->_code = \Cli::input();

            \Cli::prompt( "Введите количество для купона: " );
            \Cli::newLine();
            $this->_count = \Cli::input();

            
            \Cli::prompt( "Вы ввели:" );
            \Cli::newLine();
            \Cli::prompt( "Название  - " );
            \Cli::prompt( $this->_name );
            \Cli::newLine();
            \Cli::prompt( "Код  - " );
            \Cli::prompt( $this->_code );
            \Cli::newLine();
            \Cli::prompt( "Количество  - " );
            \Cli::prompt( $this->_count );
            \Cli::newLine();
            \Cli::prompt( "Сохранить (y/n)?" );

            if ( \Cli::input() == "y" ) {
                $sql = "INSERT INTO `coupon`(`name`, `code`, `count`, `status`, `deleted`) VALUES ('". $this->_name ."','". $this->_code ."','". $this->_count ."','1','0')";
                if ( $this->testing ) {
                    $this->db_test->query( $sql );
                    \Cli::newLine();
                    \Cli::prompt( "Создание записи в тестовой БД" );
                }
        
                \Cli::newLine();
                \Cli::prompt( "Создание записи в БД" );
                $this->db->query( $sql );
            }
            $this->_name = "";
            $this->_code = "";
            $this->_count = "";

            \Cli::clear();

            \Cli::prompt( "Добавить еще один купон (y/n)?" );
            \Cli::newLine();
            if ( \Cli::input() != "y" ) {
                $this->init = false;
                \Cli::prompt( "Добавление купона завершено" );
                \Cli::newLine();
            }

        }


        
    }

    

}
