<?php

use Cli\Commands\CommandInterface;
use \App\Payment\Payment as PaymentBank;

class Payment implements CommandInterface
{
    public $name = 'Payment';
    public $arguments = [];
    public $options = [];
    public $description = 'Команда для работы с платежами';
    
    private $db;
    private $db_test;
    private $testing;

    private $init;

    private $command_list_payment = [
        "users" => "Показать пользователей запросивших вывод средств",
        "pay" => "Выполнить перевод",
        "exit" => "Для выхода из команды",
    ];

    public function __construct() {
        global $db;
        global $db_test;
        global $testing;
        $this->db = $db;
        $this->db_test = $db_test;
        $this->testing = $testing;
        $this->init = TRUE;
    }

    public function help()
    {
        \Cli::prompt( $this->description );
    }
    
    public function run()
    {
        while ($this->init) {
            $this->getCommands();
            \Cli::prompt( "Введите команду: " );
            \Cli::newLine();
            $command = \Cli::input();

            switch (ucfirst($command)) {
                case 'Users':
                    $this->tabUser();
                    break;
                case 'Pay':
                    $count = 0;
                    while ( $count <= 0 ) {
                        \Cli::prompt( "Скольким пользователям перевезти средства (Должно быть больше нуля): " );
                        \Cli::newLine();
                        $count = (int)\Cli::input();
                        \Cli::newLine();
                    }
                    $this->paymentStart( $count );
                    break;
                case 'Exit':
                    $this->init = false;
                    break;
                
                default:
                    # code...
                    break;
            }
            
            //\Cli::clear();


        }
    }

    public function getCommands(){
        \Cli::newLine();
        \Cli::prompt("Список команд");
        foreach ($this->command_list_payment  as $key => $value) {
            \Cli::newLine();
            \Cli::prompt("    - " . $key . ":" );
            \Cli::newLine();
            \Cli::prompt("      " . $value );
            \Cli::newLine();
        }
    }

    public function findUsers( $limit = 20 )
    {
        $sql = "SELECT payments.payment_id, users.name, users_prize.count, payments.bank, payments.card 
                    FROM `payments` 
                    LEFT JOIN users ON payments.user_id = users.user_id 
                    LEFT JOIN users_prize ON payments.user_prize_id  = users_prize.user_prize_id 
                    WHERE payments.result = 'wait'
                    LIMIT $limit; ";
        
        $result = $this->db->query( $sql );
        if ( $result->num_rows ) {
            return $result->rows;
        }
        \Cli::newLine();
        \Cli::prompt("Не найдено пользователей для вывода средств");
        return false;
    }

    public function tabUser()
    {
        $result = $this->findUsers();
        if ( !$result ) {
            return false;
        }
        \Cli::newLine();
        \Cli::prompt("   Пользователь   |   Сумма   |    Банк    |    Номер карты   |");
        foreach ($result  as $key => $value) {
            \Cli::newLine();
            \Cli::prompt("   ". $value['name'] ."   |   ". $value['count'] ."    |    ". $value['bank'] ."     |    ". $value['card'] ."    |");
        }
        
    }

    public function paymentStart( $count )
    {
        $result = $this->findUsers($count);
        if ( !$result ) {
            return false;
        }
        $payment = new PaymentBank();

        foreach ($result as $key => $value) {
            \Cli::newLine();
            \Cli::prompt( "Ищем " . $value['bank']  );
            $Class = $payment->findClass( $value['bank'] );
            \Cli::newLine();
            \Cli::prompt( "Оператор $Class" );
            \Cli::newLine();
            if ( $Class ) {
                $class = new $Class;

                if ( $class->connected() ) {
                    if ( $class->payment($value['card'], $value['count']) ) {
                        $sql = "UPDATE `payments` SET result = 'success' WHERE payment_id = " . $value['payment_id'];
                        $this->db->query( $sql );
                        \Cli::newLine();
                        \Cli::prompt("Пользователю ". $value['name'] ." отправлено ". $value['count'] ." руб. на карту   ". $value['card'] ."  Банка ". $value['bank'] );
                        continue;
                    }
                    $sql = "UPDATE `payments` SET result = 'error' WHERE payment_id = " . $value['payment_id'];
                    $this->db->query( $sql );
                }
            }
            \Cli::newLine();
            \Cli::prompt("ОШИБКА!!!" );
            \Cli::newLine();
            \Cli::prompt("Пользователю ". $value['name'] ." не отправлены средства ");
        }
        
    }

    

}
