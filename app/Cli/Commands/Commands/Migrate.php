<?php



class Migrate 
{
    public $name = 'Migrate';
    public $arguments = [];
    public $options = [];
    public $description = 'Мигрирует таблицы в БД';
    
    private $key = '';
    private $autoincriment = false;
    private $fields = [];
    private $table = '';

    private $db;
    private $db_test;
    private $testing;


    public function __construct() {
        global $db;
        global $db_test;
        global $testing;
        $this->db = $db;
        $this->db_test = $db_test;
        $this->testing = $testing;
    }

    public function help()
    {
        \Cli::prompt( $this->description );
    }
    
    public function run()
    {
        $dir    = __DIR__ . '/Migrate';
        $files = scandir($dir);
        if (empty($files)){
            return false;
        }
        
        foreach ($files as $file){
            echo $file;
            if (in_array($file,array(".",".."))){
                continue;
            }
            require_once __DIR__ . '/Migrate/' . $file;
            $n_file = explode( '.', $file )[0];
            $className = "Migrate" . ucfirst($n_file);
            if (!class_exists( $className)){
                \Cli::newLine();
                \Cli::prompt( "Класс не найден" );
                continue;
            }
            
            try{
                \Cli::newLine();
                \Cli::prompt( "Инициализация класса" );
                $class = new \ReflectionClass($className);
                if (! $class->isInstantiable() ){
                    continue;
                }

                $class = new $className();
                $class->up();
            }
            catch (\ReflectionException $e){
                \Cli::newLine();
                \Cli::prompt( "Инициализация класса - ошибка" );
                $this->logger['error'] = $e->getMessage();
                \Cli::prompt( $this->logger['error'] );
            }
        }
    }

    protected function addField( array $data = [] )
    {
        $this->fields[ key($data) ] = $data[key($data)];
    }

    protected function addKey( $key = '', $autoincriment = false )
    {
        $this->key = $key;
        $this->autoincriment = $autoincriment;
    }

    protected function createTable( $table = '' )
    {
        $this->table = $table;

        if ( empty( $this->table ) ) {
            return false;
        }
        $sql = "SHOW TABLES LIKE '". $this->table ."'";
        
        $_result = false;
        $_result_test = false;
        if ( $this->testing ) {
            $result = $this->db_test->query( $sql );
            \Cli::newLine();
            \Cli::prompt( "Проверка наличия таблицы в тестовой БД" );
            if ( $result->num_rows ) {
                $_result_test = true;
            }
        }
        $result = $this->db->query( $sql );
        \Cli::newLine();
        \Cli::prompt( "Проверка наличия таблицы в БД" );
        if ( $result->num_rows ) {
            $_result = true;
        }

        if ( $_result && $_result_test ) {
            \Cli::newLine();
            \Cli::prompt( "Таблица уже создана" );
            return false;
        }
        
        $sql = "CREATE TABLE  `". $this->table ."` (";
        $fields = [];
        foreach ($this->fields as $key => $value) {
            $autoincriment = '';
            if ( $key == $this->key && $this->autoincriment ) {
                $autoincriment = ' AUTO_INCREMENT ';
            }
            $null = ' NOT NULL';
            if ( isset( $value['is_null']) ) {
                if ( $value['is_null'] === true ) {
                    $null = ' NULL';
                }
            }
            $fields[] = '`'.$key.'` ' . strtoupper($value['type']) . '( '. $value['constraint'] .' ) ' . $null . $autoincriment;
        }
        $sql .= implode(',', $fields);
        if ( $this->key ) {
            $sql .= ', PRIMARY KEY (  `'.$this->key.'` ) ';
        }
        $sql .= ');';

        if ( $this->testing && !$_result_test ) {
            $this->db_test->create( $sql );
            \Cli::newLine();
            \Cli::prompt( "Создание таблицы в тестовой БД" );
        }
        $res = '';
        if ( !$_result ) {
            \Cli::newLine();
            \Cli::prompt( "Создание таблицы БД" );
            $res = $this->db->create( $sql );
        }

        return $res;
    }

    

}
