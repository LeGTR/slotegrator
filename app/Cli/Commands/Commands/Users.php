<?php 


class Users 
{
    public $name = 'Users';
    public $arguments = [];
    public $options = [];
    public $description = 'Добавление пользователя';
    
    private $_name;
    private $_surname;
    private $_patronymic;
    private $_mail;
    private $_password;
    
    private $db;
    private $db_test;
    private $testing;

    private $init;

    public function __construct() {
        global $db;
        global $db_test;
        global $testing;
        $this->db = $db;
        $this->db_test = $db_test;
        $this->testing = $testing;
        $this->init = TRUE;
    }

    public function help()
    {
        \Cli::prompt( $this->description );
    }
    
    public function run()
    {
        while ($this->init) {
            \Cli::prompt( "Введите Имя: " );
            \Cli::newLine();
            $this->_name = \Cli::input();
            
            \Cli::prompt( "Введите Фамилию: " );
            \Cli::newLine();
            $this->_surname = \Cli::input();

            \Cli::prompt( "Введите Отчество: " );
            \Cli::newLine();
            $this->_patronymic = \Cli::input();

            \Cli::prompt( "Введите Почту: " );
            \Cli::newLine();
            $this->_mail = \Cli::input();
            while ( empty( $this->_mail ) ) {
                \Cli::prompt( "Почта обязательна: " );
                \Cli::newLine();
                $this->_mail = \Cli::input();
            }
            
            \Cli::prompt( "Введите пароль: " );
            \Cli::newLine();
            $this->_password = \Cli::input();
            while ( empty( $this->_password ) ) {
                \Cli::prompt( "пароль не может быть пустым: " );
                \Cli::newLine();
                $this->_password = \Cli::input();
            }

            \Cli::newLine();
            \Cli::prompt( "Сохранить (y/n)?" );

            if ( \Cli::input() == "y" ) {
                $sql = "INSERT INTO `users`(`name`, `surname`, `patronymic`, `mail`, `password`, `status`, `deleted`) 
                VALUES ('". $this->_name ."','". $this->_surname ."','". $this->_patronymic ."','". $this->_mail ."','". password_hash( $this->password, PASSWORD_DEFAULT ) ."','1','0')";
                if ( $this->testing ) {
                    $this->db_test->query( $sql );
                    \Cli::newLine();
                    \Cli::prompt( "Создание записи в тестовой БД" );
                }
        
                \Cli::newLine();
                \Cli::prompt( "Создание записи в БД" );
                $this->db->query( $sql );
            }
            $this->_name = "";
            $this->_surname = "";
            $this->_patronymic = "";
            $this->_mail = "";
            $this->_password = "";

            \Cli::clear();

            \Cli::prompt( "Добавить еще одного пользователя (y/n)?" );
            \Cli::newLine();
            if ( \Cli::input() != "y" ) {
                $this->init = false;
                \Cli::prompt( "Добавление пользователя завершено" );
                \Cli::newLine();
            }

        }


        
    }

    

}
