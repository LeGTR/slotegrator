<?php


class Prizeitem 
{
    public $name = 'Prizeitem';
    public $arguments = [];
    public $options = [];
    public $description = 'Добавляет приз в список случайных призов';
    
    private $_prize_id;
    private $_name;

    private $db;
    private $db_test;
    private $testing;


    private $init;

    public function __construct() {
        global $db;
        global $db_test;
        global $testing;
        $this->db = $db;
        $this->db_test = $db_test;
        $this->testing = $testing;
        $this->init = TRUE;
    }

    public function help()
    {
        \Cli::prompt( $this->description );
    }
    
    public function run()
    {

        $_prize = $this->select_prize();
        if ( !$_prize ) {
            return false;
        }
        while ($this->init) {
            \Cli::prompt( "Введите номер приза (необходимо ввести номер): " );
            $this->viewList($_prize);
            $this->_prize_id = \Cli::input();

            \Cli::prompt( "Введите название приза: " );
            \Cli::newLine();
            $this->_name = \Cli::input();

            \Cli::newLine();
            \Cli::prompt( "Сохранить (y/n)?" );

            if ( \Cli::input() == "y" ) {
                $sql = "INSERT INTO `prize_items`(`prize_id`, `name`, `status`, `deleted`) VALUES ('". $this->_prize_id ."','". $this->_name ."','1','0')";
                if ( $this->testing ) {
                    $this->db_test->query( $sql );
                    \Cli::newLine();
                    \Cli::prompt( "Создание записи в тестовой БД" );
                }
        
                \Cli::newLine();
                \Cli::prompt( "Создание записи в БД" );
                $this->db->query( $sql );
            }
            $this->_prize_id = "";
            $this->_type = "";
            $this->_name = "";

            \Cli::clear();

            \Cli::prompt( "Добавить еще один приз (y/n)?" );
            \Cli::newLine();
            if ( \Cli::input() != "y" ) {
                $this->init = false;
                \Cli::prompt( "Добавление призов завершено" );
                \Cli::newLine();
            }

        }

    }

    public function select_prize()
    {
        $sql = "SELECT * FROM `prize` WHERE type = 3";
        $prize = $this->db->query( $sql );
        $_prize = [];
        if ( !$prize->num_rows ) {
            \Cli::prompt( "Сперва необходимо добавить приз: " );
            return false;
        }
        foreach ($prize->rows as $key => $value) {
            $_prize[$value['prize_id']] = $value['name'];
        }

        return $_prize;
    }

    public function viewList( array $_prize = [])
    {
        foreach ($_prize as $key => $value) {
            \Cli::newLine();
            \Cli::prompt( "$key - $value" );
        }
        \Cli::newLine(2);
    }

    

}
