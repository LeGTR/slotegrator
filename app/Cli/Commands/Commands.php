<?php
namespace Cli\Commands;

class Commands
{
    protected $commands = [];
    protected $logger = null;

    public function __construct(){
    }

    public function run(string $command, array $params){
        $this->discoverCommands();
        $this->params = $params;

        if ( isset($this->commands[$command])){
            \CLI::newLine();
            \CLI::error("Команда $command уже создана");
            \CLI::newLine();
            return;
        }

        $this->commands[$command] = [
            'class'       => ucfirst($command),
            'arguments'   => $params['arguments'],
            'options'     => $params['options'],
            'description' => $params['description'],
        ];        

        return true;
    }

    public function getCommands(){
        $this->discoverCommands();

        return $this->commands;
    }

    public function discoverCommands(){
        if (! empty($this->commands)){
            return;
        }
        $dir    = __DIR__ . '/Commands';
        $files = scandir($dir);
        
        if (empty($files)){
            return;
        }

        foreach ($files as $file){
            if (in_array($file,array(".","..")) || is_dir(__DIR__ . '/Commands/' . $file) ){
                continue;
            }
            require_once __DIR__ . '/Commands/' . $file;
            $n_file = explode( '.', $file )[0];
            $className = ucfirst($n_file);
            
            if (empty($className) || !class_exists($className)){
                continue;
            }
            try{
                $class = new \ReflectionClass($className);
                if (! $class->isInstantiable() ){
                    continue;
                }

                $class = new $className();

                $this->commands[$class->name] = [
                    'class'       => $className,
                    'file'        => $file,
                    'arguments'   => $class->arguments,
                    'options'     => $class->options,
                    'description' => $class->description,
                ];

                $class = null;
                unset($class);
            }
            catch (\ReflectionException $e){
                $this->logger['error'] = $e->getMessage();
            }
        }

        asort($this->commands);
    }

    public function start( $name )
    {
        $_class = $this->commands[ucfirst($name)]['class'];
        $class = new $_class();
        return $class->run();
    }
}