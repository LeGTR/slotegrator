<?php

function is_cli(){
    
    if (php_sapi_name() !== 'cli') {
        return false;
    }
    return true;
}


require_once __DIR__ . "/../vendor/autoload.php";
require_once __DIR__ . '/Commands/CommandInterface.php';
require_once __DIR__ . '/Commands/Commands.php';
require_once __DIR__ . '/../Payment/PaymentInterface.php';
require_once __DIR__ . '/../Payment/Payment.php';

class CLI
{
    public static $readline_support = false;
    public $wait_msg = 'Нажмите любую кнопку для продолжения';
    protected $init = false;

    protected $delim;
    protected $delim_options;

    public $command;
    public $arguments = [];
    public $options = [];
    public $description = '';
    protected $lastWrite;

    protected $commands;
    public $command_list = [];

    protected $escape = "   ";
    protected $dash = "-";
    
    public function init($argc, $argv){
        if (is_cli()){
            $this->delim = ','; // разделитель нескольких параметров
            $this->delim_options = '='; // разделитель нескольких параметров

            self::$readline_support = extension_loaded('readline');
            $this->commands = new Cli\Commands\Commands();
            $this->command_list = $this->commands->getCommands();
            $this->init = true;
            $this->parseCommandLine($argc, $argv);
        }else{
            define('STDOUT', 'php://output');
        }
    }

    public function wait()
    {
        $this->clear();
        $this->getHelp();
        while ( $this->init ) {
            $this->prompt("Список команд");
            $this->newLine();
            $this->getCommands();
            $this->newLine(3);
            $this->prompt("Введите команду");
            
            $input = explode(' ', $this->input());

            if ( isset($this->command_list[ ucfirst($input[0]) ] ) ) {
                $this->newLine();
                $this->prompt("Команда найдена");
                $this->infoCommand( $input[0] );
            
                if ( isset( $input[1] ) ) {
                    
                    switch ( $input[1] ) {
                        case 'help':
                        case '{help}':
                            $this->getCommandHelp($input[0]);
                            break;
            
                        case 'start':
                        case '{start}':
                            $this->commandStart($input[0]);
                            break;
                        default:
                            $this->prompt("Команда не определена");
                            break;
                    }
            
                }
            }else{
                switch ( $input[0] ) {
                    case 'help':
                    case '{help}':
                        $this->getHelp();
                        break;
        
                    case 'list':
                    case '{list}':
                        $this->getCommands();
                        break;
                    case 'exit':
                    case '{exit}':
                        $this->init = false;
                        break;
                    default:
                        $this->prompt("Команда не определена2");
                        break;
                }
            }
            $this->newLine();
        }
    }

    protected function commandStart( $name = '' )
    {
        $this->commands->start($name);
    }
    
    protected function parseCommandLine($argc, $argv){

        if ( !isset( $argv[1] ) ) {
            return $this->wait();
        }
        
        $this->command = $argv[1];

        for ($i = 1; $i < $argc; $i++) {
            if (preg_match('/\[(.+?)\]/', $argv[$i], $match)){
                $this->inputOptions( $match );
            }
            if (preg_match('/^{(.+?)}/', $argv[$i], $match)){
                $this->inputArguments( $match );
            }
        }
    }

    
    protected function inputArguments($match){
        
        // если в значение аргумента есть разделитель
        if( strpos( $match[1], $this->delim ) !== false ){
            // преобразуем значение в массив из этих аргументов
            foreach (explode( $this->delim, $match[1] ) as $key => $value) {
                $this->arguments[] = $value;
            }
        }else{
            $this->arguments[] = $match[1];
        }

    }

    
    protected function inputOptions($argv){

        $_match = explode($this->delim_options, $argv[1]);
        if ( isset( $_match[1] ) ) {
            // регулярным выражением проверяем, есть ли нужное нам соответствие
            if (preg_match('/^{(.+?)}/', $_match[1], $match)){
                // если в значение аргумента есть разделитель
                if( strpos( $match[1], $this->delim ) !== false ){
                    // преобразуем значение в массив из этих аргументов
                    foreach (explode( $this->delim, $match[1] ) as $key => $value) {
                        $this->options[$_match[0]][] = $value;
                    }
                }else{
                    $this->options[$_match[0]][] = $match[1];
                }
            }else{
                $this->options[$_match[0]] = $_match[1];
            }
        }
        
    }

    public function description( $description ){
        $this->description = $description;
    }

    public static function input(string $prefix = null): string{
        if (self::$readline_support)
        {
            return readline($prefix);
        }

        echo $prefix;

        return fgets(STDIN);
    }

    public static function prompt(string $field, $options = null, string $validation = null): string{
        $extra_output = ' ';

        self::fwrite(STDOUT, $field . $extra_output);

        return empty($input) ? '' : $input;
    }
    
    protected static function fwrite($handle, string $string){
        if (is_cli()){
            fwrite($handle, $string);
            return;
        }

        echo $string;
    }
    public static function error(string $text){
        static::fwrite(STDERR, $text . PHP_EOL);
    }

    public static function newLine(int $num = 1){
        for ($i = 0; $i < $num; $i ++){
            static::fwrite( STDERR, PHP_EOL );
        }
    }

    public static function clear(int $num = 1){
        system('cls');
        system('clear');
    }

    public function getHelp(){
        $this->newLine();
        $this->prompt("Команда {LIST} покажет список всех комманд в системе");
        $this->newLine();
        $this->prompt("Команда {EXIT} заканчивает программу");
        $this->newLine();
        $this->prompt("Команда command_name выведет данные команды");
        $this->newLine();
        $this->prompt("Команда command_name {HELP} покажет поисание определенной команды");
    }

    public function getCommands(){
        $this->newLine();
        $this->prompt("Список команд");
        foreach ($this->command_list  as $key => $value) {
            $this->newLine();
            $this->prompt($this->escape . $this->dash . $value['class'] . " ($key)");
        }
    }

    public function getCommandHelp( $name = '' ){
        if ( empty($name) ) {
            return false;
        }
        $this->newLine();
        $this->prompt("Описание команды $name:");
        if( isset($this->command_list[ucfirst($name)]) ){
            $this->newLine();
            $this->prompt($this->escape . $this->dash . $this->command_list[ucfirst($name)]['description']);
        }
    }

    public function infoCommand( $name ){
        if ( isset( $this->command_list[ucfirst($name)] ) ) {
            $this->newLine();
            $this->prompt("Called command: $name");
            $this->newLine();
            if ( isset( $this->command_list[$name]['arguments'] ) ) {
                $this->newLine();
                $this->prompt("Arguments: ");
                foreach ($this->command_list[$name]['arguments'] as $key => $value) {
                    $this->newLine();
                    $this->prompt($this->escape . $this->dash . " $value");
                }
            }
            
            if ( isset( $this->command_list[$name]['options'] ) ) {
                $this->newLine();
                $this->prompt("Options: ");
                foreach ($this->command_list[$name]['options'] as $key => $value) {
                    if ( is_array( $value ) ) {
                        $this->newLine();
                        $this->prompt($this->escape . $this->dash . " $key");
                        foreach ($value as $key => $value2) {
                            $this->newLine();
                            $this->prompt($this->escape . $this->escape . $this->dash . " $value2");
                        }
                    }else{
                        $this->newLine();
                        $this->prompt($this->escape . $this->dash . " $key");
                        $this->newLine();
                        $this->prompt($this->escape . $this->escape . $this->dash . " $value");
                    }
                }
            }

        }else{
            $this->newLine();
            $this->prompt("Команда не найдена");
        }
    }

}
