<?php namespace App\Filter;

use Core\Filters\Filter;
use Core\Filters\FilterInterface;

class Prize extends Filter implements FilterInterface
{ 
    
    public function start()
    {
        if ( !isset( $_SESSION['user'] ) ) {
            $_SESSION['errors'] = ["user" => "Пользователь не авторизован"];

            return false;
        }
        $_SESSION['prize'] = false;
        $prize = new \App\Models\UsersPrizeModel();
        $_prize = $prize->getItem( $_SESSION['user']['user_id'] );

        if ( $_prize ) {
            $_SESSION['prize'] = $_prize['prize_id'];

            return false;
        }

    }

    public function init()
    {

    }

    public function after()
    {

    }
}
