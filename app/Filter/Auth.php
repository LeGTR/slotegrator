<?php namespace App\Filter;

use Core\Filters\Filter;
use Core\Filters\FilterInterface;

class Auth extends Filter implements FilterInterface
{ 
    
    public function start()
    {
        //var_dump( $this->db );
        $_SESSION['auth'] = false;
        if ( isset( $_SESSION['user'] ) ) {
            $_SESSION['auth'] = true;
        }
    }

    public function init()
    {

    }

    public function after()
    {

    }
}
