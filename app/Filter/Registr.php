<?php namespace App\Filter;

use Core\Filters\Filter;
use Core\Filters\FilterInterface;

use App\Models\UsersModel;

class Registr extends Filter implements FilterInterface
{ 
    
    public function start()
    {
        //var_dump( $this->db );
        $_SESSION['_POST'] = $_POST;
        if ( !empty( $_POST['mail'] ) ) {
            $user = new UsersModel();
            $result = $user->getItem($_POST['mail']);

            if ( $result ) {
                $_SESSION['errors']["mail"] = "Почта занята";
            }
        }

        if ( empty( $_POST['password'] ) || empty( $_POST['password_repeat'] ) ) {
            $_SESSION['errors']["password"] = "Поле пароль обязательное";
        }elseif( $_POST['password'] != $_POST['password_repeat'] ){
            $_SESSION['errors']["password"] = "Пароли не совпадают";
        }

        $_POST['password'] = password_hash( $_POST['password'], PASSWORD_DEFAULT );
        unset( $_POST['password_repeat'] );

        if ( !isset( $_POST['status'] ) || empty( $_POST['status'] ) ) {
            $_POST['status'] = 1;
        }
        if ( !isset( $_POST['deleted'] ) || empty( $_POST['deleted'] ) ) {
            $_POST['deleted'] = 0;
        }
    }

    public function init()
    {

    }

    public function after()
    {

    }
}
