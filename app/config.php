<?php

$config = [];

$config["default_controller"] = "Home";
$config["default_method"] = "index";

$config["min_money"] = 10;
$config["max_money"] = 100;

$config["min_bonus"] = 10;
$config["max_bonus"] = 100;
/**Рабочая БД */
$config["database"]["default"]["host"] = "db";
$config["database"]["default"]["user"] = "root";
$config["database"]["default"]["password"] = "12345";
$config["database"]["default"]["table"] = "slotegrator";
$config["database"]["default"]["port"] = "3306";


/**тестовая БД */
$config["database"]["tests"]["host"] = "db";
$config["database"]["tests"]["user"] = "root";
$config["database"]["tests"]["password"] = "12345";
$config["database"]["tests"]["table"] = "slotegrator_test";
$config["database"]["tests"]["port"] = "3306";

return $config;